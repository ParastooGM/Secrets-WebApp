# Secrets WebApp

This is a dockerized web application created with NodeJS, EJS, Express, and mongoDB; and PassportJS is used to implement local login, Google OAuth and Facebook OAuth strategies. This is a website where people can sign up to read or post secrets.

Currently hosted on https://secrets-web-app-nu8d.onrender.com
