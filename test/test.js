const request = require("supertest");
const { expect } = require("expect");
const { app } = require("../app");
require("dotenv").config();

describe("Testing GET /secrets endpoint without authentication.", () => {
  it("respond with valid HTTP status code and results in a redirect.", async () => {
    const response = await request(app).get("/secrets");

    expect(response.status).toBe(302);
  });
});


describe("Testing POST /login endpoint with an already existing user.", () => {
  it("respond with valid HTTP status code and results in a redirect.", async () => {
    const response = await request(app)
      .post("/login")
      .set('content-type',"application/x-www-form-urlencoded")
      .send({
        username: process.env.DEFAULT_EMAIL,
        password: process.env.DEFAULT_PASSWORD,
      })

    expect(response.status).toBe(302);
  });
});
